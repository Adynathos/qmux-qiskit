
jupyter nbconvert qmux.ipynb --to markdown --output qmux.md
# pandoc qmux.md --standalone --mathjax --template page_template.html --metadata title='QMux' -o qmux_pandoc.html
# mjpage < qmux_pandoc.html > index.html

pandoc qmux.md --standalone --mathjax --template page_template.html --metadata title='QMux' | mjpage > index.html

rm qmux.md
