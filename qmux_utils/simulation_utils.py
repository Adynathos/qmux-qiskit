
import numpy as np
from IPython.display import display

from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit, execute, BasicAer
from qiskit.compiler import transpile
from qiskit.transpiler import PassManager
from qiskit.transpiler.passes import Optimize1qGates

simulator_qasm = BasicAer.get_backend('qasm_simulator')
simulator_unitary = BasicAer.get_backend('unitary_simulator')

def run_circuit_probabilistic(qcircuit, num_samples=10000):
	""" Simulate the execution of the circuit, return the frequencies of each bitstring result """
	result = execute(qcircuit, simulator_qasm, shots=num_samples).result()
	return {
		bitstring: count / num_samples 
		for bitstring, count in sorted(result.get_counts().items())
	}


def run_circuit_unitary(qcircuit):
	""" Simulate to get circuit's unitary matrix """
	result = execute(qcircuit, simulator_unitary).result()
	return result.get_unitary(qcircuit)


def result_to_str(result_dict):
	return '	'.join(f'{bitstring}: {frac:.3f}' for bitstring, frac in sorted(result_dict.items()))


def try_1_qubit_operation(*gates, print_label=None):
	""" Execute the list of gates """
	qreg = QuantumRegister(1)
	creg = ClassicalRegister(1)
	qcirc = QuantumCircuit(qreg, creg)

	# add all gates acting on the single qubit
	for gate in gates:
		qcirc.append(gate, [qreg[0]])
	
	# get unitary
	result_unitary = run_circuit_unitary(qcirc)

	# measure and 
	qcirc.measure(qreg[0], creg[0])
	result_counts = run_circuit_probabilistic(qcirc)

	if print_label is not None:
		print(f'--- {print_label} ---\n{result_unitary}\nsim: {result_to_str(result_counts)}')

	return dict(
		unitary = result_unitary,
		counts = result_counts,
	)



def try_2_qubit_operation_get_unitary(*gate_defs):
	qreg = QuantumRegister(2)
	qcirc = QuantumCircuit(qreg)

	for item in gate_defs:
		if isinstance(item, (list, tuple)):
			gate, register_indices = item
		else:
			gate = item
			register_indices = [1, 0]
		qcirc.append(gate, [qreg[i] for i in register_indices])

	# optimize
	qcirc = PassManager(Optimize1qGates()).run(transpile(qcirc, basis_gates=['u1', 'u3', 'u2', 'cx']))

	# get unitary
	unitary = run_circuit_unitary(qcirc)

	return unitary

def try_2_qubit_operation_get_counts(*gate_defs):
	qreg = QuantumRegister(2)
	creg = ClassicalRegister(2)
	qcirc = QuantumCircuit(qreg, creg)

	qcirc.h(qreg[1])

	for item in gate_defs:
		if isinstance(item, (list, tuple)):
			gate, register_indices = item
		else:
			gate = item
			register_indices = [1, 0]
		qcirc.append(gate, [qreg[i] for i in register_indices])

	# measure and 
	qcirc.measure(qreg[0], creg[0])
	qcirc.measure(qreg[1], creg[1])

	# optimize
	qcirc = PassManager(Optimize1qGates()).run(transpile(qcirc, basis_gates=['u1', 'u3', 'u2', 'cx']))

	return run_circuit_probabilistic(qcirc)


def unitary_remove_phase(A):
	if np.abs(A[0, 0]) > 1e-4:
		return A / A[0, 0]
	else:
		return A

def unitary_difference(A, B):
	return np.linalg.norm(unitary_remove_phase(A) - unitary_remove_phase(B), ord='fro')


def try_2_qubit_operation(*gate_defs, print_label=None, gt_unitary=None):
	"""
	@param gt_unitary: ground truth unitary, we will compare to that
	"""
	result_unitary = try_2_qubit_operation_get_unitary(*gate_defs)
	result_counts = try_2_qubit_operation_get_counts(*gate_defs)

	if gt_unitary is not None:
		unitary_diff = unitary_difference(gt_unitary, result_unitary)

	if print_label is not None:
		msg = f'--- {print_label} ---\n{result_unitary}\nsim: {result_to_str(result_counts)}'
		if gt_unitary is not None:
			msg += f'\nunitary difference: {unitary_diff:.03f}'
		print(msg)

	return dict(
		unitary = result_unitary,
		counts = result_counts,
	)


def print_circuit_stats(circ, msg=''):
	print('{msg}{s}ops: {ops}	| depth: {d}'.format(
		msg = msg,
		s = ' ' if msg else '',
		d = circ.depth(),
		ops = ', '.join(f'{count} {name}' for name, count in sorted(circ.count_ops().items())),
	))
	# print(f'{msg} ops: {circ.count_ops()} depth: {circ.depth()}')


def draw_gate_2q(g, disp=True, disp_decomposed=None, disp_optimized=False, save=None, save_decomposed=None):
	disp_decomposed = disp_decomposed if disp_decomposed is not None else disp

	# construct circuit with our gate
	qc = QuantumRegister(1, 'c')
	qt = QuantumRegister(1, 't')
	qcirc = QuantumCircuit(qc, qt)

	qcirc.append(g, [qc[0], qt[0]])

	# decompose once to see the gate's internals
	fig = qcirc.decompose().draw(output='mpl')
	if save:
		fig.savefig(save)
	if disp:
		display(fig)

	# show circuit as it is made of fundamental gates
	if disp_decomposed or save_decomposed or disp_optimized:
		qcirc_decomposed = transpile(qcirc, basis_gates=['u1', 'u3', 'u2', 'cx'])

		fig_decomp = qcirc_decomposed.draw(output='mpl')

		if save_decomposed:
			fig_decomp.savefig(save_decomposed)

		if disp_decomposed:
			# print(f'ops: {qcirc_decomposed.count_ops()} depth: {qcirc_decomposed.depth()}')
			print_circuit_stats(qcirc_decomposed, 'Decomposed:')
			display(fig_decomp)

		if disp_optimized:
			qcirc_opt = PassManager(Optimize1qGates()).run(qcirc_decomposed)
			print_circuit_stats(qcirc_opt, 'Optimized:')
			display(qcirc_opt.draw(output='mpl'))
