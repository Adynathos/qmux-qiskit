from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister
from qiskit.circuit import Gate
from IPython.display import display
from pathlib import Path

# qiskit/visualization/matplotlib.py : 751

# elif op.name.startswith('custom_control_'):
# 	disp = op.name.replace('custom_control_', '')
# 	self._ctrl_qubit(q_xy[0])
# 	self._gate(q_xy[1], wide=_iswide, text=disp)
# 	self._line(qreg_b, qreg_t)


# qiskit/visualization/matplotlib.py : 219

# import re
# text_no_latex = text
# if '$' in text_no_latex:
# 	text_no_latex = re.sub(r'\\\w+', 'X', text_no_latex)
# 	text_no_latex = re.sub(r'[\$_\^\{\}]', '', text_no_latex)

# boxes_wide = round(len(text_no_latex) / 10) or 1
# wid = WID * 3.5 * boxes_wide

def disp_and_save(qcirc, save, disp):
	fig = qcirc.draw(output='mpl')
	if save: 
		Path(save).parent.mkdir(parents=True, exist_ok=True)
		fig.savefig(save)
		
	if disp:
		display(fig)
		
	return fig

def draw_qmux_definition(save=None, disp=True):
	qc = QuantumRegister(1, 'c')
	qt = QuantumRegister(1, 't')
	qcirc = QuantumCircuit(qc, qt)

	qcirc.append(Gate('custom_control_$U_0$', 2, []), [qc[0], qt[0]])
	qcirc.append(Gate('custom_control_$U_1$', 2, []), [qc[0], qt[0]])

	return disp_and_save(qcirc, save, disp)
	

def draw_qmux_composed(save=None, disp=True):
	qc = QuantumRegister(1, 'c')
	qt = QuantumRegister(1, 't')
	qcirc = QuantumCircuit(qc, qt)

	qcirc.append(Gate('$U_0$', 1, []), [qt[0]])
	qcirc.append(Gate('custom_control_$U_1 {U_0}^\dagger$', 2, []), [qc[0], qt[0]])

	return disp_and_save(qcirc, save, disp)


def draw_diagonalization(save=None, disp=True):
	qc = QuantumRegister(1, 'c')
	qt = QuantumRegister(1, 't')
	qcirc = QuantumCircuit(qc, qt)

		
	qcirc.append(Gate('$V_0$', 1, []), [qt[0]])
	qcirc.cx(qc[0], qt[0])
	qcirc.append(Gate(r'$R_z(\frac{1}{2}(\theta_1+\theta_2))$', 1, []), [qc[0]])
	qcirc.append(Gate(r'$R_z(\frac{1}{2}(\theta_1-\theta_2))$', 1, []), [qt[0]])
	qcirc.cx(qc[0], qt[0])
	qcirc.append(Gate('$V_1$', 1, []), [qt[0]])
	
	return disp_and_save(qcirc, save, disp)


def draw_test_env(save=None, disp=True):
	qc = QuantumRegister(1, 'c')
	qt = QuantumRegister(1, 't')
	creg = ClassicalRegister(2, 'out')
	qcirc = QuantumCircuit(qc, qt, creg)

	qcirc.h(qc[0])
	qcirc.append(Gate('QMux', 2, []), [qc[0], qt[0]])
	qcirc.measure(qc[0], creg[0])
	qcirc.measure(qt[0], creg[1])

	return disp_and_save(qcirc, save, disp)




from qiskit.compiler import transpile
from qiskit.transpiler import PassManager
from qiskit.transpiler.passes import Optimize1qGates

def circuit_stats(circ):
	print(f'ops: {circ.count_ops()} depth: {circ.depth()}')

def test_transpile():
	q = QuantumRegister(2, 'q')
	circ = QuantumCircuit(q)
	circ.append(QMuxCompose([1, 1, 1], [2, 2, 2]), [q[0], q[1]])
	#circ.append(QMuxCompose([0, 0, 0.5], [0, 0, 1.]), [q[0], q[1]])

	circ_decomp = transpile(circ, basis_gates=['u1', 'u3', 'u2', 'cx'])
	circuit_stats(circ_decomp)
	display(circ_decomp.draw(output='mpl'))
	
	circ_opt = PassManager(Optimize1qGates()).run(circ_decomp)
	circuit_stats(circ_opt)
	display(circ_opt.draw(output='mpl'))
		
#test_transpile()



if __name__ == '__main__':
	draw_qmux_definition(save='fig/qmux_definition_edit.svg')
	draw_qmux_composed(save='fig/qmux_compose_1.svg')
	draw_diagonalization(save='fig/qmux_diagonalization_edit.svg')
	draw_test_env(save='fig/test_env.svg')
